'use strict'

const express = require('express')
const api = express.Router()
const userCtrl = require('../controllers/user')
const esbCtrl = require('../controllers/esb')
const tokenService = require('../services/getToken')

api.post('/post/registro', userCtrl.registerUser)
api.post('/post/iniciar', userCtrl.logIn)
api.post('/post/locations', userCtrl.updateLocations)
api.get('/get/localizacion/:idUsuario', userCtrl.getLocations)
api.post('/post/agregarTraduccionCadena', tokenService, esbCtrl.postAgregarTraduccionCadena);

api.get('/get/complementos', tokenService, esbCtrl.getComplementos)
api.get('/get/complemento/:complementoId', tokenService, esbCtrl.getComplemento)
api.get('/get/catalogo/:complementoId', tokenService, esbCtrl.getCatalogoComplemento)
api.post('/post/nuevoComplemento', tokenService, esbCtrl.postComplemento)
api.post('/post/AprobarTraduccionCadena', tokenService, esbCtrl.postAprobarTraduccionCadena)
api.delete('/delete/complemento/:complementoId', tokenService, esbCtrl.deleteComplemento)

module.exports = api
