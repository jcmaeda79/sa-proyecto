const http = require('http')

const esb_host = process.env.ESB_HOST;
const esb_port = process.env.ESB_PORT;
const ms_almacenamiento_host = process.env.MS_ALMACENAMIENTO_HOST;
const ms_almacenamiento_port = process.env.MS_ALMACENAMIENTO_PORT;
const ms_almacenamiento = process.env.MS_ALMACENAMIENTO;
const path = '/post/comunicacion';

function postComplemento(token, nombre, correo, complemento, next){
    if(!token){
        next(null, err);
    }

    var clientData = JSON.stringify ({
        "token": token,
        "url" : "http://" + ms_almacenamiento_host + ":" + ms_almacenamiento_port +"/post/complemento",
        "tipo" : "POST",
        "funcionSolicitada" : ms_almacenamiento + ".guardarComplemento",
        "parametros" : {
            "token" : token,
            "nombre" : nombre,
            "correo" : correo,
            "complemento" : complemento
        }
    });

    console.log('ESB client connection data: ');
    console.log(clientData);

    var options = {
        host: esb_host,
        port: esb_port,
        path: path,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': clientData.length
        }
    }

    invocarServicio(options, clientData, function(msg, err){
        if(err)
            next(null, err)
        else
            next(msg, null)
    })

    /*var msg = {
        "estado": "200",
        "mensaje": "OK",
        "data": {
            "nombre": "nombreComplemento",
            "path": "pathCompleto"
        }
    }

    next(msg, null)*/

}

function getComplementos(token, next){
    
    if(!token){
        next(null, err)
    }
    
    var clientData = JSON.stringify ({
        "token": token,
        "url" : "http://" + ms_almacenamiento_host + ":" + ms_almacenamiento_port +"/get/complementos?token="+token,
        "tipo" : "GET",
        "funcionSolicitada" : ms_almacenamiento + ".obtenerComplemento",
        "parametros": {
            "token": token
        }
    });

    console.log("--------------------------------")
    console.log(clientData)
    console.log("--------------------------------")

    var options = {
        host: esb_host,
        port: esb_port,
        path: path,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': clientData.length
        }
    }

    invocarServicio(options, clientData, function(msg, err){
        if(err)
            next(null, err)
        else
            next(msg, null)
    })

    /*var msg = {
        "estado": "200",
        "mensaje": "OK",
        "data":{
            "complementos":[
                {
                    "nombreComplemento": "nombreComplemento1",
                    "idComplemento":"1",
                    "localizacionOriginal":"en_us",
                    "nombre": "usuario1",
                    "correo": "correo1@correo.com",
                    "localizaciones": [
                        "es_ar",
                        "es_us"
                    ]
                },
                {
                    "nombreComplemento": "nombreComplemento2",
                    "idComplemento":"2",
                    "localizacionOriginal":"en_us",
                    "nombre": "usuario1",
                    "correo": "correo1@correo.com",
                    "localizaciones": [
                        "es_ar",
                        "es_us"
                    ]
                }
            ]
        }
    }

    next(msg, null)*/

}

function getComplemento(token, complementoId, next) {
    if(!token){
        next(null, err)
    }


    var clientData = JSON.stringify ({
        "token": token,
        "url" : "http://" + ms_almacenamiento_host + ":" + ms_almacenamiento_port +"/get/complemento/"+complementoId+"?token="+token,
        "tipo" : "GET",
        "funcionSolicitada" : ms_almacenamiento + ".obtenerComplemento",
        "parametros": {
            "token": token
        }
           
    });

    var options = {
        host: esb_host,
        port: esb_port,
        path: path,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': clientData.length
        }
    }

    invocarServicio(options, clientData, function(msg, err){
        if(err)
            next(null, err)
        else
            next(msg, null)
    })

    /*const msg = {
        "estado": "200",
        "mensaje": "OK",
        "data": {
            "nombreComplemento": "nombreComplemento" + complementoId,
            "idComplemento": complementoId,
            "localizacionOriginal": "en_us",
            "nombre": "nombre de usuario",
            "correo": "correo",
            "localizaciones": ["es_ar", "es_us"],
            "cadenas": [
                "cadena original 1",
                "cadena original 2",
                "cadena original 3",
                "cadena original 4",
                "cadena original 5"
            ]
        }
    }
    next(msg, null);*/
}

function getCatalogoComplemento(token, complementoId, next) {
    if(!token){
        next(null, err)
    }

    var clientData = JSON.stringify ({
        "token": token,
        "url" : "http://" + ms_almacenamiento_host + ":" + ms_almacenamiento_port + "/get/catalogo/"+complementoId+"?token="+token,
        "tipo" : "GET",
        "funcionSolicitada" : ms_almacenamiento + ".obtenerCatalogo",
        "parametros": {
            "token": token
        }
    });

    console.log('----------------------------------------------------------------------')
    console.log(clientData)
    console.log('----------------------------------------------------------------------')

    var options = {
        host: esb_host,
        port: esb_port,
        path: path,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': clientData.length
        }
    }

    console.log('llamando al ESB : get catalogo complemento');
    console.log(clientData);
    invocarServicio(options, clientData, function(msg, err){
        if(err)
            next(null, err)
        else
            next(msg, null)
    })
    
    /*const msg = {
        "estado": "200",
        "mensaje": "OK",
        "data": {
            "idComplemento": complementoId,
            "nombre": "nombreComplemento" + complementoId,
            "catalogo" : [
                {
                    "localizacionOriginal": "en_us",
                    "localizacionTraduccion": "es_ar",
                    "contenido": [
                        {
                            "msgid": "Edit",
                            "msgstr": "Editar",
                            "numberoAprobaciones": 0
                        },
                        {
                            "msgid": "Edit",
                            "msgstr": "Editar",
                            "numberoAprobaciones": 0
                        },
                        {
                            "msgid": "Edit",
                            "msgstr": "Editar",
                            "numberoAprobaciones": 0
                        }
                    ]
                },
                {
                    "localizacionOriginal": "en_us",
                    "localizacionTraduccion": "es_es",
                    "contenido": [
                        {
                            "msgid": "Edit",
                            "msgstr": "Editar",
                            "numberoAprobaciones": 0
                        },
                        {
                            "msgid": "Edit",
                            "msgstr": "Editar",
                            "numberoAprobaciones": 0
                        },
                        {
                            "msgid": "Edit",
                            "msgstr": "Editar",
                            "numberoAprobaciones": 0
                        }
                    ]
                }
            ]
        }
    }

    next(msg, null);*/
}

function postAprobarTraduccionCadena(token, nombreComplemento, idComplemento, nombreLocalizacionOriginal, nombreLocalizacionTraducida, cadenaOriginal, cadenaTraducida, nombre, 
    correo, next){
    if(!token){
        next(null, err)
    }
    
    var clientData = JSON.stringify ({
        "token": token,
        "url" : "http://" + ms_almacenamiento_host + ":" + ms_almacenamiento_port +"/post/aprobarTraduccionCadena",
        "tipo" : "POST",
        "funcionSolicitada" : ms_almacenamiento + ".aprobarTraduccionCadena",
        "parametros" : {
            "token" : token,
            "nombreComplemento" : nombreComplemento,
            "idComplemento": idComplemento,
            "nombreLocalizacionOriginal" : nombreLocalizacionOriginal,
            "nombreLocalizacionTraducida" : nombreLocalizacionTraducida,
            "cadenaOriginal" : cadenaOriginal,
            "cadenaTraducida" : cadenaTraducida,
            "nombre": nombre,
            "correo": correo
        }
    });

    console.log('ESB client connection data: ');
    console.log(clientData);

    var options = {
        host: esb_host,
        port: esb_port,
        path: path,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': clientData.length
        }
    }

    invocarServicio(options, clientData, function(msg, err){
        if(err)
            next(null, err)
        else
            next(msg, null)
    })

    /*var msg = {
        "estado": "200",
        "mensaje": "revision exitosa",
    }

    next(msg, null)*/

}

function postAgregarTraduccionCadena(token, nombreComplemento, idComplemento, nombreLocalizacionOriginal, nombreLocalizacionTraducida, cadenaOriginal, cadenaTraducida, nombre, 
    correo, next){
    
    if(!token){
        next(null, err)
    }
    
    var clientData = JSON.stringify ({
        "token": token,
        "url" : "http://" + ms_almacenamiento_host + ":" + ms_almacenamiento_port +"/post/agregarTraduccionCadena",
        "tipo" : "POST",
        "funcionSolicitada" : ms_almacenamiento + ".agregarTraduccionCadena",
        "parametros" : {
            "token" : token,
            "nombreComplemento" : nombreComplemento,
            "idComplemento": idComplemento,
            "nombreLocalizacionOriginal" : nombreLocalizacionOriginal,
            "nombreLocalizacionTraducida" : nombreLocalizacionTraducida,
            "cadenaOriginal" : cadenaOriginal,
            "cadenaTraducida" : cadenaTraducida,
            "nombre": nombre,
            "correo": correo
        }
    });

    console.log('ESB client connection data: ');
    console.log(clientData);

    var options = {
        host: esb_host,
        port: esb_port,
        path: path,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': clientData.length
        }
    }

    invocarServicio(options, clientData, function(msg, err){
        if(err)
            next(null, err)
        else
            next(msg, null)
    })

}

function deleteComplemento(token, complementoId, next){
    if(!token){
        next(null, err)
    }

    var clientData = JSON.stringify ({
        "token": token,
        "url" : "http://" + ms_almacenamiento_host + ":" + ms_almacenamiento_port + "/delete/complemento/"+complementoId+"?token="+token,
        "tipo" : "DELETE",
        "funcionSolicitada" : ms_almacenamiento + ".eliminarComplemento",
        "parametros": {
            "token": token
        }
    });

    var options = {
        host: esb_host,
        port: esb_port,
        path: path,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': clientData.length
        }
    }

    invocarServicio(options, clientData, function(msg, err){
        if(err)
            next(null, err)
        else
            next(msg, null)
    })
}
/*
* Función 'invocarServicio' que consume los servicios RESTful y devuelve
* el JSON de respuesta.
*/

function invocarServicio(options, jsonObject, next){
    var req = http.request(options, function(res){
        var contentType = res.headers['content-type']
        /*
        * 'data' almacena los datos del servicio RESTful
        */
        var data = ''

        res.on('data', function(chunk){
            data += chunk
        }).on('end', function(){
            /*
            * Se finaliza la recepción de información y se procesa 
            */
           console.log(data)
            var response = data

            /*
            * Se valida que sea JSON 
            */

            if(contentType.indexOf('application/json') != -1) {
                response = JSON.parse(data)
            }

            next(response, null)
        }).on('error', function(err){
            console.error('Error al procesar la solicitud ' + err)
        }).on('uncaughtException', function(err){
            console.error('Excepcion ' + err)
        })
    }).on('error', function(err){
        console.error('HTTP request fallida ' + err)
        next(null, err)
    })

    if(jsonObject)
        req.write(jsonObject)

    req.end()
}

module.exports = {
    postComplemento,
    getComplementos,
    getComplemento,
    getCatalogoComplemento,
    postAprobarTraduccionCadena,
    postAgregarTraduccionCadena,
    deleteComplemento
}