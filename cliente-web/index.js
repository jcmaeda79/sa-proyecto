const express = require('express');
const exphbs = require('express-handlebars');
const path = require('path');
const axios = require('axios');

const app = express();

app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

// body parser
app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.disable('etag');


let loggedUser = undefined;


const MS_TRADUCCIONES_DOMAIN = process.env.MS_TRADUCCIONES_DOMAIN

app.get('/', (req, res) => {

    if (loggedUser) {
        res.redirect('/inicio');
    } else {
        res.redirect('/login')
    }

});


app.get('/registro', (req, res) => {
    res.render('registro', {
    });
});

app.post('/registro', (req, res) => {

    const nombre = req.body.nombre;
    const correo = req.body.correo;
    const password = req.body.password;
    console.log('REGISTRO');
    console.log('nombre: ' + nombre);
    console.log('correo: ' + correo);
    console.log('password: ' + password);
    console.log('MS_TRADUCCIONES_DOMAIN: ' + MS_TRADUCCIONES_DOMAIN + '/post/registro');

    axios.post(MS_TRADUCCIONES_DOMAIN + '/post/registro', {
        nombre: nombre,
        correo: correo,
        password: password
    })
    .then((response) => {
        console.log(`statusCode: ${response.status}`)
        console.log(response.data.message)
        if (response.status === 200) {
            res.redirect('/login')
        }
    })
    .catch((error) => {
        console.error(error.response.data);
        res.render('registro', {
            error: error.response.data.message
        });
    });

});

app.get('/login', (req, res) => {
    res.render('login', {
    });
});

app.post('/login', (req, res) => {

    const email = req.body.email;
    const password = req.body.password;
    console.log('LOGIN');
    console.log('email: ' + email);
    console.log('password: ' + password);
    console.log('MS_TRADUCCIONES_DOMAIN: ' + MS_TRADUCCIONES_DOMAIN + '/post/iniciar');

    axios.post(MS_TRADUCCIONES_DOMAIN + '/post/iniciar', {
        correo: email,
        password: password
    })
    .then((response) => {
        console.log(`statusCode: ${response.status}`)
        console.log(response.data.message)
        if (response.status === 200) {
            loggedUser = {
                correo: email
            }
            res.redirect('/inicio');
        }
    })
    .catch((error) => {
        console.error(error.response.data);
        res.render('login', {
            error: error.response.data.message
        });
    });

});

app.get('/inicio', (req, res) => {

    console.log('Obteniendo complementos');
    axios.get(MS_TRADUCCIONES_DOMAIN + '/get/complementos', {})
    .then((response) => {
        console.log(`statusCode: ${response.status}`)
        console.log(response.data)
        if (response.data.estado === '200') {
            console.log(response.data.data.complementos)
            res.render('inicio', {
                loggedUser: loggedUser,
                complementos: response.data.data.complementos
            });
        }
    })
    .catch((error) => {
        console.error(error);
        res.render('inicio', {
            loggedUser: loggedUser,
            error: error.response.data      // TODO este escenario no está probado
        });
    });    
});


app.get('/complemento/:id', (req, res) => {

    const complementoId = req.params.id;

    console.log('Obteniendo catalogo complementos');
    axios.get(MS_TRADUCCIONES_DOMAIN + '/get/catalogo/'+complementoId, {})
    .then((response) => {
        console.log(`statusCode: ${response.status}`)
        console.log(response.data)
        console.log(response.data.data)
        if (response.status === 200) {
            res.render('complemento', {
                loggedUser: loggedUser,
                data: response.data.data
            });
        }
    })
    .catch((error) => {
        console.error(error);
        res.render('complemento', {
            loggedUser: loggedUser,
            error: error.response.data      // TODO este escenario no está probado
        });
    });
});

app.get('/logout', (req, res) => {
    loggedUser = undefined;
    res.redirect('/login');
});

// static folder
app.use(express.static(path.join(__dirname, 'public')));

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Express server startd on port ${PORT}`));