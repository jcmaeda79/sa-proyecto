var jwt = require('jsonwebtoken')
const moment = require('moment')
var fs = require('fs')
var path = require('path')
/**
 * Funciòn que verifica si el token no ha expirado y que sea valido.
 */
function tokenValidation(tokenParam){
    var publickey = fs.readFileSync('/home/publickeys/publickey.ssh', 'utf8');
    let token = '';
    console.log(tokenParam);
    if (Array.isArray(tokenParam)) {
        token=tokenParam[0];
    } else {
        token = tokenParam;
    }
    var decoded = jwt.verify(token, publickey,function(err, decoded) {
        if (err) {
            console.log(err);
            err = {
                estado: '500',
                mensaje: 'El token ha expirado',
              }
              return err;        
          }
          return decoded
    });
    if(decoded.auth == 'true' && decoded.exp <= moment().add(2, 'minutes').unix()){
        console.log(decoded)
        return true;
    }
    return decoded;
              

}

module.exports = {
    tokenValidation
}